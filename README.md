# simpleNN

[![pipeline status](https://gitlab.cern.ch/mschille/simpleNN/badges/master/pipeline.svg)](https://gitlab.cern.ch/mschille/simpleNN/commits/master) 

## aims

`simpleNN` is a first shot to see if it's possible to implement low overhead
RNNs for the inclusive FT in LHCb. The idea is to convert the NN to C++ that is
then compiled. The resulting code should run reasonably fast, and not use
dynamic memory during normal operation (it's okay in initialisation, but once
you start feeding data, things should happen exclusively on the stack). Also,
the NN structure should be obvious to a human being reading the code to
facilitate use and debugging. `simpleNN` therefore employs a data-flow oriented
design, making heavy use of overloaded `operator>>` and `operator<<` to
highlight the flow of data.

## features

While `simpleNN` is a work in progress, it already implements enough
infrastructure to be useful. There is a code generator, [RNN
generator](https://gitlab.cern.ch/vjevtic/rnngenerator), to convert NN
architectures trained with keras into `simpleNN` C++ code. 

Currently, `simpleNN` supports these constructs:

- supports simple dense and GRU layers
- activation functions:
  * linear
  * minimum, maximum, clamp to given range
  * sigmoid
  * hard sigmoid
  * exponential
  * hyperbolic tangent
  * ReLU
  * LeakyReLU
  * softsign
  * softplus
  * elu
  * selu
  * softmax

Transcendental functions in activation functions can be autovectorised by
leveraging [libfvm](https://gitlab.cern.ch/mschille/libfvm). To aid
benchmarking, there are preprocessor defines that

- disable the code that instructs the compiler to try really hard to
  autovectorise, one has to compile with `-DSIMPLENN_NOVECTORIZE` (if you want
  to be sure, you also have to remove optimisation flags that permit
  autovectorisation)
- disable `libfvm`'s fast transcendental functions, and fall back on the C++
  standard library ones, one has to compile with `-DSIMPLENN_NOLIBFVM`

## TODO

Lots, actually. If you have a bright idea, need something, or have code to
contribute, please get in touch.
