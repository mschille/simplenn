/** @file include/simplenn/span.h
 *
 * @brief span (view) of (a consecutive portion) of an array
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2019-08-08
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include <cassert>
#include <iterator>
#include <algorithm>

#include "cppcompat.h"

namespace simpleNN {
    /// implementation details for span
    namespace span_impl {
        /// policy class: span size known at run time
        class runtime_size {
            private:
                const std::size_t m_sz;
            public:
                constexpr runtime_size(std::size_t sz) noexcept : m_sz(sz) {}
                constexpr std::size_t size() const noexcept { return m_sz; }
        };

        /// policy class: span size known at compile time
        template <std::size_t SZ>
        class compiletime_size {
            private:
                static constexpr std::size_t s_sz = SZ;
            public:
                constexpr compiletime_size(std::size_t /* unused */) noexcept {}
                constexpr std::size_t size() const noexcept { return s_sz; }
        };
    } // namespace span_impl

    /// helper for make_span, see below
    template <typename FLT, typename SZCLASS>
    class const_span : public SZCLASS {
    protected:
        const FLT* m_begin;

    public:
        using value_type = FLT;
        using size_type = std::size_t;
        using difference_type = std::ptrdiff_t;
        using reference = const FLT&;
        using const_reference = const FLT&;
        using pointer = const FLT*;
        using const_pointer = const FLT*;
        using iterator = const_pointer;
        using const_iterator = const_pointer;

        using SZCLASS::size;

        constexpr explicit const_span(const_pointer begin,
                                      size_type sz) noexcept
                : SZCLASS(sz), m_begin(begin)
        {}

        constexpr const_iterator cbegin() const noexcept { return m_begin; }
        constexpr const_iterator cend() const noexcept
        {
            return cbegin() + size();
        }
        constexpr const_iterator begin() const noexcept { return cbegin(); }
        constexpr const_iterator end() const noexcept { return cend(); }
        iterator begin() noexcept { return cbegin(); }
        iterator end() noexcept { return cbegin() + size(); }

        constexpr const_pointer data() const noexcept { return begin(); }
        pointer data() noexcept { return begin(); }

        constexpr const_reference operator[](std::size_t idx) const noexcept
        {
            return (assert(idx < size()), begin()[idx]);
        }
    };

    /// helper for make_span, see below
    template <typename FLT, typename SZCLASS>
    class span : public const_span<FLT, SZCLASS> {
    public:
        using value_type = typename const_span<FLT, SZCLASS>::value_type;
        using size_type = typename const_span<FLT, SZCLASS>::size_type;
        using reference = FLT&;
        using pointer = FLT*;
        using iterator = pointer;

        using const_span<FLT, SZCLASS>::size;
        using const_span<FLT, SZCLASS>::begin;
        using const_span<FLT, SZCLASS>::end;
        using const_span<FLT, SZCLASS>::data;
        using const_span<FLT, SZCLASS>::operator[];

        constexpr explicit span(pointer begin, size_type sz) noexcept
                : const_span<FLT, SZCLASS>::const_span(begin, sz)
        {}

        template <typename INTYPE>
        span& operator=(const INTYPE& other) const noexcept
        {
            assert(size() == other.size());
            std::copy(other.begin(), other.end(), begin());
            return *this;
        }

        iterator begin() noexcept
        {
            return const_cast<pointer>(const_span<FLT, SZCLASS>::m_begin);
        }
        iterator end() noexcept { return begin() + size(); }
        pointer data() noexcept { return begin(); }
        reference operator[](size_type idx) noexcept
        {
            return (assert(idx < size()), begin()[idx]);
        }
    };

    /** @brief make a (const) span
     *
     * This function returns a (const) span of elements stored in a container
     * with contiguous memory layout.
     *
     * @tparam PARENT   parent container type
     *
     * @param parent    parent container
     * @param first     first index that the returned span contains
     * @param last      (one past) last index the returned span contains
     *
     * @returns a (const) span
     */
    template <typename PARENT>
    const_span<typename PARENT::value_type, span_impl::runtime_size>
    make_span(const PARENT& parent, std::size_t first,
              std::size_t last) noexcept
    {
        return const_span<typename PARENT::value_type,
                          span_impl::runtime_size>{
                (assert(first <= parent.size() && first <= last &&
                        last <= parent.size()),
                 (parent.data() + first)),
                static_cast<std::size_t>(last - first)};
    }

    /** @brief make a span
     *
     * This function returns a span of elements stored in a container
     * with contiguous memory layout.
     *
     * @tparam PARENT   parent container type
     *
     * @param parent    parent container
     * @param first     first index that the returned span contains
     * @param last      (one past) last index the returned span contains
     *
     * @returns a span
     */
    template <typename PARENT>
    span<typename PARENT::value_type, span_impl::runtime_size>
    make_span(PARENT& parent, std::size_t first, std::size_t last) noexcept
    {
        return span<typename PARENT::value_type, span_impl::runtime_size>{
                (assert(first <= parent.size() && first <= last &&
                        last <= parent.size()),
                 (parent.data() + first)),
                static_cast<std::size_t>(last - first)};
    }

    /** @brief make a (const) span
     *
     * This function returns a (const) span of elements stored between
     * two pointers.
     *
     * @tparam T        type of elements pointed to
     *
     * @param first     pointer to first element the returned span contains
     * @param last      pointer to (one past) last element the returned span
     *                  contains
     *
     * @returns a (const) span
     */
    template <typename T>
    const_span<T, span_impl::runtime_size> make_span(const T* first,
                                                     const T* last) noexcept
    {
        return const_span<T, span_impl::runtime_size>{first, last - first};
    }

    /** @brief make a span
     *
     * This function returns a span of elements stored between
     * two pointers.
     *
     * @tparam T        type of elements pointed to
     *
     * @param first     pointer to first element the returned span contains
     * @param last      pointer to (one past) last element the returned span
     *                  contains
     *
     * @returns a span
     */
    template <typename T>
    span<T, span_impl::runtime_size> make_span(T* first, T* last) noexcept
    {
        return span<T, span_impl::runtime_size>{first, last - first};
    }

    /** @brief make a (const) span
     *
     * This function returns a (const) span of elements stored in a container
     * with contiguous memory layout.
     *
     * @tparam FIRST    first index that the returned span contains
     * @tparam LAST     (one past) last index the returned span contains
     * @tparam PARENT   parent container type
     *
     * @param parent    parent container
     *
     * @returns a (const) span
     */
    template <std::size_t FIRST, std::size_t LAST, typename PARENT>
    const_span<typename PARENT::value_type,
               span_impl::compiletime_size<LAST - FIRST>>
    make_span(const PARENT& parent) noexcept
    {
        static_assert(FIRST <= LAST, "invalid span FIRST...LAST");
        return const_span<typename PARENT::value_type,
                          span_impl::compiletime_size<LAST - FIRST>>{
                (assert(FIRST <= parent.size() && LAST <= parent.size()),
                 (parent.data() + FIRST)),
                0};
    }

    /** @brief make a span
     *
     * This function returns a span of elements stored in a container
     * with contiguous memory layout.
     *
     * @tparam FIRST    first index that the returned span contains
     * @tparam LAST     (one past) last index the returned span contains
     * @tparam PARENT   parent container type
     *
     * @param parent    parent container
     *
     * @returns a span
     */
    template <std::size_t FIRST, std::size_t LAST, typename PARENT>
    span<typename PARENT::value_type,
         span_impl::compiletime_size<LAST - FIRST>>
    make_span(PARENT& parent) noexcept
    {
        static_assert(FIRST <= LAST, "invalid span FIRST...LAST");
        return span<typename PARENT::value_type,
                    span_impl::compiletime_size<LAST - FIRST>>{
                (assert(FIRST <= parent.size() && LAST <= parent.size()),
                 (parent.data() + FIRST)),
                0};
    }

    /** @brief make a (const) span
     *
     * This function returns a (const) span of elements stored between
     * two pointers.
     *
     * @tparam FIRST    first index that the returned span contains
     * @tparam LAST     (one past) last index the returned span contains
     * @tparam T        type of elements pointed to
     *
     * @param base      pointer to array
     *
     * @returns a (const) span
     */
    template <std::size_t FIRST, std::size_t LAST, typename T>
    const_span<T, span_impl::compiletime_size<LAST - FIRST>>
    make_span(const T* base) noexcept
    {
        static_assert(FIRST <= LAST, "invalid span FIRST...LAST");
        return const_span<T, span_impl::compiletime_size<LAST - FIRST>>{
                base + FIRST, 0};
    }

    /** @brief make a span
     *
     * This function returns a span of elements stored between
     * two pointers.
     *
     * @tparam FIRST    first index that the returned span contains
     * @tparam LAST     (one past) last index the returned span contains
     * @tparam T        type of elements pointed to
     *
     * @param base      pointer to array
     *
     * @returns a span
     */
    template <std::size_t FIRST, std::size_t LAST, typename T>
    span<T, span_impl::compiletime_size<LAST - FIRST>>
    make_span(const T* base) noexcept
    {
        static_assert(FIRST <= LAST, "invalid span FIRST...LAST");
        return span<T, span_impl::compiletime_size<LAST - FIRST>>{
                base + FIRST, 0};
    }
} // namespace simpleNN

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
