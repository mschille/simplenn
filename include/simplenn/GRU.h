/** @file include/simlpenn/GRU.h
 *
 * @brief a GRU layer
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @author Vukan Jevtic <vukan.jevtic@cern.ch>
 * @date 2019-08-08
 *
 * For copyright and license information, see the end of the file.
 */

#pragma once

#include <array>
#include <vector>
#include <cassert>
#include <algorithm>

#include "cppcompat.h"
#include "processing.h"
#include "EmbeddedOutputVector.h"

namespace simpleNN {
    enum GRUKind {
        GRUv1 = 0, GRUv2
    };
    template <std::size_t NOUT, std::size_t NIN, typename ACT = procs::tanh,
              typename REC_ACT = procs::hard_sigmoid, typename FLT = float,
              GRUKind KIND = GRUv1>
    class GRU : public OutputEmbedder<GRU<NOUT, NIN, ACT, REC_ACT, FLT, KIND>,
                                      FLT, NOUT> {
    private:
        std::vector<FLT> m_bz;
        procs::matrix_cruncher<NOUT, NOUT, FLT> m_uz;
        procs::matrix_cruncher<NOUT, NIN, FLT> m_wz;
        std::vector<FLT> m_br;
        procs::matrix_cruncher<NOUT, NOUT, FLT> m_ur;
        procs::matrix_cruncher<NOUT, NIN, FLT> m_wr;
        std::vector<FLT> m_bh;
        procs::matrix_cruncher<NOUT, NOUT, FLT> m_uh;
        procs::matrix_cruncher<NOUT, NIN, FLT> m_wh;

        // v_i = fn(v_i);
        template <typename V, typename FN>
        static inline void apply(V& v, FN&& fn) noexcept
        {
            std::transform(v.begin(), v.end(), v.begin(),
                           std::forward<FN>(fn));
        }
        // v_i = v_i + w_i
        template <typename V, typename W>
        static inline void add_elwise(V& v, const W& w) noexcept
        {
            auto it = w.cbegin();
            std::transform(
                    v.begin(), v.end(), v.begin(),
                    [&it](const FLT& el) noexcept { return el + *it++; });
        }
        // v_i = v_i * w_i
        template <typename V, typename W>
        static inline void mult_elwise(V& v, const W& w) noexcept
        {
            auto it = w.cbegin();
            std::transform(
                    v.begin(), v.end(), v.begin(),
                    [&it](const FLT& el) noexcept { return el * *it++; });
        }

    public:
        template <typename ITBZ, typename ITUZ, typename ITWZ, typename ITBR,
                  typename ITUR, typename ITWR, typename ITBH, typename ITUH,
                  typename ITWH>
        constexpr GRU(ITBZ bzfirst, ITBZ bzlast, ITUZ uzfirst, ITUZ uzlast,
                      ITWZ wzfirst, ITUZ wzlast, ITBR brfirst, ITBR brlast,
                      ITUR urfirst, ITUR urlast, ITWR wrfirst, ITUR wrlast,
                      ITBH bhfirst, ITBH bhlast, ITUH uhfirst, ITUH uhlast,
                      ITWH whfirst, ITUH whlast)
                : m_bz((assert(NOUT == std::distance(bzfirst, bzlast)),
                        bzfirst),
                       bzlast),
                  m_uz(uzfirst, uzlast), m_wz(wzfirst, wzlast),
                  m_br((assert(NOUT == std::distance(brfirst, brlast)),
                        brfirst),
                       brlast),
                  m_ur(urfirst, urlast), m_wr(wrfirst, wrlast),
                  m_bh((assert(NOUT == std::distance(bhfirst, bhlast)),
                        bhfirst),
                       bhlast),
                  m_uh(uhfirst, uhlast), m_wh(whfirst, whlast)
        {}

        template <typename FLT2,
                  typename = typename std::enable_if<
                          std::is_floating_point<FLT2>::value>::type>
        constexpr GRU(const std::initializer_list<FLT2> bz,
                      const std::initializer_list<FLT2> uz,
                      const std::initializer_list<FLT2> wz,
                      const std::initializer_list<FLT2> br,
                      const std::initializer_list<FLT2> ur,
                      const std::initializer_list<FLT2> wr,
                      const std::initializer_list<FLT2> bh,
                      const std::initializer_list<FLT2> uh,
                      const std::initializer_list<FLT2> wh)
                : GRU(bz.begin(), bz.end(), uz.begin(), uz.end(), wz.begin(),
                      wz.end(), br.begin(), br.end(), ur.begin(), ur.end(),
                      wr.begin(), wr.end(), bh.begin(), bh.end(), uh.begin(),
                      uh.end(), wh.begin(), wh.end())
        {}
        constexpr GRU(const std::vector<FLT>& bz, const std::vector<FLT>& uz,
                      const std::vector<FLT>& wz, const std::vector<FLT>& br,
                      const std::vector<FLT>& ur, const std::vector<FLT>& wr,
                      const std::vector<FLT>& bh, const std::vector<FLT>& uh,
                      const std::vector<FLT>& wh)
                : GRU(bz.begin(), bz.end(), uz.begin(), uz.end(), wz.begin(),
                      wz.end(), br.begin(), br.end(), ur.begin(), ur.end(),
                      wr.begin(), wr.end(), bh.begin(), bh.end(), uh.begin(),
                      uh.end(), wh.begin(), wh.end())
        {}
        constexpr GRU(std::vector<FLT>&& bz, std::vector<FLT>&& uz,
                      std::vector<FLT>&& wz, std::vector<FLT>&& br,
                      std::vector<FLT>&& ur, std::vector<FLT>&& wr,
                      std::vector<FLT>&& bh, std::vector<FLT>&& uh,
                      std::vector<FLT>&& wh)
                : m_bz((assert(NOUT == bz.size()), std::move(bz))),
                  m_uz(std::move(uz)), m_wz(std::move(wz)),
                  m_br((assert(NOUT == br.size()), std::move(br))),
                  m_ur(std::move(ur)), m_wr(std::move(wr)),
                  m_bh((assert(NOUT == bh.size()), std::move(bh))),
                  m_uh(std::move(uh)), m_wh(std::move(wh))
        {}

        // the only bit that does something...
        template <typename OUT, typename IN>
        void operator()(OUT& out, const IN& x) const noexcept
        {
            assert(x.size() == NIN);
            alignas(64) std::array<FLT, NOUT> z, r, h;
            std::copy(m_bz.begin(), m_bz.end(), z.begin());
            std::copy(m_br.begin(), m_br.end(), r.begin());
            switch (KIND) {
            case GRUv1: std::copy(m_bh.begin(), m_bh.end(), h.begin()); break;
            case GRUv2: h.fill(FLT(0)); break;
            default: assert(false);
            }
            m_uz(z, out);
            m_wz(z, x);
            m_ur(r, out);
            m_wr(r, x);
            apply(z, REC_ACT());
            apply(r, REC_ACT());
            switch (KIND) {
            case GRUv1:
                mult_elwise(r, out);
                m_uh(h, r);
                break;
            case GRUv2:
                m_uh(h, out);
                mult_elwise(h, r);
                add_elwise(h, m_bh);
                break;
            default: assert(false);
            }
            m_wh(h, x);
            apply(h, ACT());
            mult_elwise(out, z);
            std::transform(z.begin(), z.end(), z.begin(),
                           [](const FLT& el) noexcept { return 1 - el; });
            {
                auto it = z.cbegin(), jt = h.cbegin();
                std::transform(out.begin(), out.end(), out.begin(),
                               [&it, &jt](const FLT& el) noexcept {
                                   return procs::math::fma(*it++, *jt++, el);
                               });
            }
        }
    };
} // namespace simpleNN

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
