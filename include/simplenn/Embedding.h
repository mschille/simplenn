/** @file include/simplenn/Embedding.h
 *
 * @brief an embedding layer
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-03-12
 *
 * For copyright and license information, see the end of the file.
 */

#pragma once

#include <tuple>
#include <vector>
#include <cassert>
#include <utility>
#include <algorithm>

#include "cppcompat.h"
#include "processing.h"
#include "EmbeddedOutputVector.h"

namespace simpleNN {
    /// implementation details
    namespace impl {
        // forward decl
        template <typename T> struct rank;
        
        // implementation details of rank
        template <typename T>
        std::integral_constant<std::size_t, 0>
        rank_impl(const T&, unsigned short);
        template <typename T>
        std::integral_constant<std::size_t,
            1 + rank<decltype(std::declval<const T&>()[0])>::value>
        rank_impl(const T&, unsigned);
        
        /// rank of a type (how often you can "index" with [])
        template <typename T>
        struct rank : decltype(rank_impl(std::declval<const T&>(), 0u)) {};
        
        // check implementation
        static_assert(0 == rank<int>::value, "int");
        static_assert(1 == rank<int[1]>::value, "int[1]");
        static_assert(1 == rank<std::array<int, 3>>::value,
                "std::array<int, 3>");
        static_assert(1 == rank<std::vector<int>>::value,
                "std::vector<int>");
        static_assert(2 == rank<int[2][3]>::value, "int[2][3]");
    } // namespace impl

    /** @brief an embedding layer
     *
     * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
     *         (with the help of Vukan Jevtic's pseudocode)
     * @date 2020-03-12
     *
     * An embedding layer turns a vector of indices into a vector of floats
     * by replacing each element in the input vector with an output vector,
     * concatenating these output vectors to form the final output.
     *
     * @tparam OUTSZ     size of the output vector
     * @tparam INSZ      size of the input vector of indices
     * @tparam MAXIDX    size of "alphabet", i.e. the number of valid values
     *                   for the index
     * @tparam IDXVECTSZ size of the vector
     * @tparam FLT       floating point type
     * @tparam IDX       type for the index variable
     */
    template <std::size_t OUTSZ, std::size_t INSZ, std::size_t MAXIDX,
             std::size_t IDXVECTSZ, typename FLT = float,
             typename IDXTYPE = unsigned>
    class Embedding : public OutputEmbedder<
                      Embedding<OUTSZ, INSZ, MAXIDX, IDXVECTSZ, FLT, IDXTYPE>,
                      FLT, OUTSZ>
    {
        static_assert(std::is_integral<IDXTYPE>::value &&
                std::is_unsigned<IDXTYPE>::value,
                "IDXTYPE must be an unsigned integral type");
        static_assert(INSZ > 0, "input index vector too small");
        static_assert(IDXVECTSZ > 0, "index vector too small");
        static_assert(OUTSZ == INSZ * IDXVECTSZ,
                "output size does not match expectation");
    private:
        std::vector<FLT> m_coeffs;
    public:
        /// construct from pair of iterators
        template <typename IT>
        constexpr Embedding(IT first, IT last)
            : m_coeffs((assert(IDXVECTSZ * MAXIDX ==
                               std::distance(first, last)), first), last)
        {}
        /// construct from initializer_list
        template <typename FLT2,
                  typename = typename std::enable_if<
                      std::is_floating_point<FLT2>::value>::type>
        explicit constexpr Embedding(std::initializer_list<FLT2> ilist)
            : Embedding(ilist.begin(), ilist.end())
        {}
        /// copy-construct from vector
        explicit constexpr Embedding(const std::vector<FLT>& coeffs)
            : Embedding(std::begin(coeffs), std::end(coeffs))
        {}
        /// move-construct from vector
        explicit constexpr Embedding(std::vector<FLT>&& coeffs)
            : m_coeffs((assert(coeffs.size() == IDXVECTSZ * MAXIDX),
                        std::move(coeffs)))
        {}
        /// construct from matrix
        template <typename MATTYPE, typename = void,
                  typename = typename std::enable_if<
                      2 == impl::rank<MATTYPE>::value>::type>
        explicit Embedding(const MATTYPE& arr)
        {
            assert(IDXVECTSZ * MAXIDX ==
                   std::distance(std::begin(arr), std::end(arr)) *
                   std::distance(std::begin(arr[0]), std::end(arr[1])));
            m_coeffs.reserve(IDXVECTSZ * MAXIDX);
            auto it = std::back_inserter(m_coeffs);
            for (const auto& row : arr) {
                it = std::copy(std::begin(row), std::end(row), it);
            }
            assert(IDXVECTSZ * MAXIDX == m_coeffs.size());
        }

        template <typename OUTTYPE, typename INTYPE>
        typename std::enable_if<1 == impl::rank<INTYPE>::value>::type
        operator()(OUTTYPE& out, const INTYPE& in) const noexcept
        {
            assert(INSZ == in.size());
            assert(OUTSZ == out.size());
            auto it = std::begin(out);
            for (const auto& idx : in) {
                assert(idx < MAXIDX);
                it = std::copy(&m_coeffs[IDXVECTSZ * idx],
                               &m_coeffs[IDXVECTSZ * (idx + 1)], it);
            }
        }
        template <typename OUTTYPE, typename INTYPE>
        typename std::enable_if<1 == INSZ && 0 == impl::rank<INTYPE>::value &&
                                std::is_integral<INTYPE>::value &&
                                std::is_unsigned<INTYPE>::value>::type
        operator()(OUTTYPE& out, const INTYPE& idx) const noexcept
        {
            assert(OUTSZ == out.size());
            assert(idx < MAXIDX);
            std::copy(&m_coeffs[IDXVECTSZ * idx],
                      &m_coeffs[IDXVECTSZ * (idx + 1)], std::begin(out));
        }
    };

    template <std::size_t OUTSZ, std::size_t INSZ, std::size_t MAXIDX,
             std::size_t IDXVECTSZ, typename FLT = float,
             typename IDXTYPE = unsigned, typename... ARGS>
    Embedding<OUTSZ, INSZ, MAXIDX, IDXVECTSZ, FLT, IDXTYPE>
    make_embedding(ARGS&&... args)
    {
        return Embedding<OUTSZ, INSZ, MAXIDX, IDXVECTSZ, FLT, IDXTYPE>(
                std::forward<ARGS>(args)...);
    }
} // namespace simpleNN

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
