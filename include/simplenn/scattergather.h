/** @file include/simplenn/scattergather.h
 *
 * @brief scatter/gather to fixed indices in an array
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2019-08-08
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include <array>
#include <cassert>
#include <iterator>
#include <algorithm>

#include "utils.h"

namespace simpleNN {
    namespace impl {
        template <typename FLT>
        class const_iterator {
        private:
            const FLT* m_data = nullptr;
            const std::size_t* m_it = nullptr;

        public:
            using iterator_category = std::random_access_iterator_tag;
            using value_type = FLT;
            using size_type = std::size_t;
            using difference_type = std::ptrdiff_t;
            using reference = const FLT&;
            using const_reference = const FLT&;
            using pointer = const FLT*;
            using const_pointer = const FLT*;

            constexpr const_iterator(const FLT* data,
                                     const std::size_t* it) noexcept
                    : m_data(data), m_it(it)
            {}

            bool operator!() const noexcept { return !m_data || !m_it; }
            operator bool() const noexcept { return m_data && m_it; }

            const_reference operator*() const noexcept
            {
                return m_data[*m_it];
            }
            const_pointer operator->() const noexcept
            {
                return &m_data[*m_it];
            }

            const_iterator& operator++() noexcept { return (++m_it, *this); }
            const_iterator& operator--() noexcept { return (--m_it, *this); }

            const_iterator operator++(int /* unused */) noexcept
            {
                const_iterator retVal(*this);
                operator++();
                return retVal;
            }
            const_iterator operator--(int /* unused */) noexcept
            {
                const_iterator retVal(*this);
                operator--();
                return retVal;
            }

            const_iterator& operator+=(difference_type incr) noexcept
            {
                return (m_it += incr, *this);
            }
            const_iterator& operator-=(difference_type decr) noexcept
            {
                return (m_it -= decr, *this);
            }

            friend const_iterator operator+(const const_iterator& it,
                                            difference_type incr) noexcept
            {
                const_iterator retVal(it);
                retVal += incr;
                return retVal;
            }
            friend const_iterator operator+(difference_type incr,
                                            const const_iterator& it) noexcept
            {
                return it + incr;
            }
            friend const_iterator operator-(const const_iterator& it,
                                            difference_type decr) noexcept
            {
                const_iterator retVal(it);
                retVal -= decr;
                return retVal;
            }
            friend difference_type
            operator-(const const_iterator& it,
                      const const_iterator& jt) noexcept
            {
                return (assert(it.m_data == jt.m_data), (it.m_it - jt.m_it));
            }

            friend bool operator==(const const_iterator& it,
                                   const const_iterator& jt) noexcept
            {
                return it.m_data == jt.m_data && it.m_it == jt.m_it;
            }
            friend bool operator!=(const const_iterator& it,
                                   const const_iterator& jt) noexcept
            {
                return !(it == jt);
            }
            friend bool operator<(const const_iterator& it,
                                  const const_iterator& jt) noexcept
            {
                return (it.m_data < jt.m_data) ||
                       (it.m_data == jt.m_data && it.m_it < jt.m_it);
            }
            friend bool operator>(const const_iterator& it,
                                  const const_iterator& jt) noexcept
            {
                return jt < it;
            }
            friend bool operator<=(const const_iterator& it,
                                   const const_iterator& jt) noexcept
            {
                return !(it > jt);
            }
            friend bool operator>=(const const_iterator& it,
                                   const const_iterator& jt) noexcept
            {
                return !(it < jt);
            }
        };

        template <typename FLT>
        class iterator : public const_iterator<FLT> {
        public:
            using difference_type =
                    typename const_iterator<FLT>::difference_type;
            using reference = FLT&;
            using pointer = FLT*;

            iterator(FLT* data, const std::size_t* it)
                    : const_iterator<FLT>{data, it}
            {}

            reference operator*() noexcept
            {
                return const_cast<reference>(
                        const_iterator<FLT>::operator*());
            }
            pointer operator->() noexcept
            {
                return const_cast<pointer>(const_iterator<FLT>::operator->());
            }

            iterator& operator++() noexcept
            {
                return (const_iterator<FLT>::operator++(), *this);
            }
            iterator& operator--() noexcept
            {
                return (const_iterator<FLT>::operator--(), *this);
            }
            iterator operator++(int /* unused */) noexcept
            {
                iterator retVal(*this);
                operator++();
                return retVal;
            }
            iterator operator--(int /* unused */) noexcept
            {
                iterator retVal(*this);
                operator--();
                return retVal;
            }
            iterator& operator+=(difference_type incr) noexcept
            {
                return (const_iterator<FLT>::operator+=(incr), *this);
            }
            iterator& operator-=(difference_type decr) noexcept
            {
                return (const_iterator<FLT>::operator-=(decr), *this);
            }
            iterator operator+(difference_type incr) noexcept
            {
                iterator retVal(*this);
                operator+=(incr);
                return retVal;
            }
            iterator operator-(difference_type decr) noexcept
            {
                iterator retVal(*this);
                operator-=(decr);
                return retVal;
            }
        };
    } // namespace impl

    /// helper for gather, see below
    template <typename FLT, std::size_t... IDXs>
    class gather_proxy {
    protected:
        constexpr static std::array<std::size_t, sizeof...(IDXs)> s_idx{
                {IDXs...}};
        const FLT* m_arr;

    public:
        using value_type = FLT;
        using size_type = std::size_t;
        using difference_type = std::ptrdiff_t;
        using reference = const FLT&;
        using const_reference = const FLT&;
        using pointer = const FLT*;
        using const_pointer = const FLT*;
        using iterator = impl::const_iterator<FLT>;
        using const_iterator = impl::const_iterator<FLT>;
        constexpr static size_type size() noexcept { return sizeof...(IDXs); }

        template <typename SRCTYPE>
        constexpr explicit gather_proxy(const SRCTYPE& src) noexcept
                : m_arr((assert(impl::max(IDXs...) < src.size()), src.data()))
        {}

        constexpr const_reference operator[](std::size_t idx) const noexcept
        {
            return (assert(idx < size()), m_arr[s_idx[idx]]);
        }

        constexpr const_iterator cbegin() const noexcept
        {
            return {m_arr, s_idx.data()};
        }
        constexpr const_iterator cend() const noexcept
        {
            return {m_arr, s_idx.data() + s_idx.size()};
        }
        constexpr const_iterator begin() const noexcept { return cbegin(); }
        constexpr const_iterator end() const noexcept { return cend(); }
        iterator begin() noexcept { return {m_arr, s_idx.data()}; }
        iterator end() noexcept
        {
            return {m_arr, s_idx.data() + s_idx.size()};
        }
    };

    template <typename FLT, std::size_t... IDXs>
    constexpr std::array<std::size_t, sizeof...(IDXs)>
            gather_proxy<FLT, IDXs...>::s_idx;

    /// helper for scatter, see below
    template <typename FLT, std::size_t... IDXs>
    class scatter_proxy : public gather_proxy<FLT, IDXs...> {
    public:
        using value_type = typename gather_proxy<FLT, IDXs...>::value_type;
        using size_type = typename gather_proxy<FLT, IDXs...>::size_type;
        using reference = FLT&;
        using pointer = FLT*;
        using iterator = impl::iterator<FLT>;

        using gather_proxy<FLT, IDXs...>::size;

        template <typename T>
        constexpr explicit scatter_proxy(T&& src)
                : gather_proxy<FLT, IDXs...>::gather_proxy(
                          std::forward<T>(src))
        {}

        template <typename INTYPE>
        scatter_proxy& operator=(const INTYPE& other) const noexcept
        {
            assert(size() == other.size());
            std::copy(other.begin(), other.end(), begin());
            return *this;
        }

        reference operator[](size_type idx) noexcept
        {
            return (assert(idx < size()),
                    const_cast<value_type*>(gather_proxy<FLT, IDXs...>::m_arr)
                            [gather_proxy<FLT, IDXs...>::s_idx[idx]]);
        }
        iterator begin() noexcept
        {
            return {const_cast<value_type*>(
                            gather_proxy<FLT, IDXs...>::m_arr),
                    gather_proxy<FLT, IDXs...>::s_idx.data()};
        }
        iterator end() noexcept
        {
            return {const_cast<value_type*>(
                            gather_proxy<FLT, IDXs...>::m_arr),
                    gather_proxy<FLT, IDXs...>::s_idx.data() +
                            gather_proxy<FLT, IDXs...>::s_idx.size()};
        }
    };

    /** @brief gather some indices from an array
     *
     * Usage example:
     * @code
     * std::vector<float> vec = get_from_somewhere();
     * // do something in a function that needs access to
     * // { vec[1], vec[3], vec[5], vec[7] }
     * do_something(gather<1, 3, 5, 7>(vec));
     * @endcode
     *
     * gather returns a proxy object that allows read-only access to the
     * specified indices (without copying any data).
     */
    template <std::size_t... IDXs, typename SRCTYPE>
    constexpr gather_proxy<typename SRCTYPE::value_type, IDXs...>
    gather(const SRCTYPE& in) noexcept
    {
        static_assert(sizeof...(IDXs) != 0, "need at least one index");
        return gather_proxy<typename SRCTYPE::value_type, IDXs...>(in);
    }

    /** @brief scatter values from a smaller into a bigger array
     *
     * @code
     * std::array<float, 20> bigarray;
     * std::array<float, 5> smallarray = from_somewhere();
     * // write to indices 0, 1, 3, 7, 4
     * scatter<0, 1, 3, 7, 4>(bigarray) = smallarray;
     * @endcode
     */
    template <std::size_t... IDXs, typename DESTTYPE>
    constexpr scatter_proxy<typename DESTTYPE::value_type, IDXs...>
    scatter(DESTTYPE& out) noexcept
    {
        static_assert(sizeof...(IDXs) != 0, "need at least one index");
        return scatter_proxy<typename DESTTYPE::value_type, IDXs...>(out);
    }
} // simpleNN

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
