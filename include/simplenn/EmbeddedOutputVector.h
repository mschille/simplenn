/** @file include/simplenn/EmbeddedOutputVector.h
 *
 * @brief machinery for "swallowing" output vectors, and streaming notation
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2019-08-08
 *
 * For copyright and license information, see the end of the file.
 */

#pragma once

#include <array>
#include <cassert>
#include <utility>
#include <algorithm>
#include <type_traits>

#include "cppcompat.h"
#include "utils.h"

namespace simpleNN {
    namespace EmbeddedOutputVector_impl {
        template <typename T, typename = void>
        struct has_static_size : std::false_type {};
        template <typename T>
        struct has_static_size<T, cppcompat::void_t<decltype(T::size())>>
                : std::true_type {};

        template <typename T, typename = void>
        struct has_constexpr_size : std::false_type {};
        template <typename T>
        struct has_constexpr_size<
                T,
                cppcompat::void_t<std::integral_constant<std::size_t, T::size()>>>
                : std::true_type {};

    } // namespace EmbeddedOutputVector_impl

    template <typename V, typename PARENT>
    class EmbeddedOutputVector {
    protected:
        alignas(std::is_reference<V>::value ? alignof(void*) : 64) V m_out;
        PARENT m_parent;

        using VV = typename std::remove_cv<
                typename std::remove_reference<V>::type>::type;

    public:
        using size_type = typename VV::size_type;
        using difference_type = typename VV::difference_type;
        using value_type = typename VV::value_type;
        using reference = typename VV::reference;
        using const_reference = typename VV::const_reference;
        using pointer = typename VV::pointer;
        using const_pointer = typename VV::const_pointer;
        using iterator = typename VV::iterator;
        using const_iterator = typename VV::const_iterator;

        constexpr EmbeddedOutputVector(
                PARENT parent,
                typename std::remove_reference<V>::type& out) noexcept
                : m_out(out), m_parent(parent)
        {}
        constexpr EmbeddedOutputVector(
                PARENT parent,
                typename std::remove_reference<V>::type&&
                        out) noexcept(noexcept(VV(std::move(out))))
                : m_out(std::move(out)), m_parent(parent)
        {}

        template <
                typename Other,
                typename = typename std::enable_if<std::is_same<
                        value_type, typename Other::value_type>::value>::type>
        EmbeddedOutputVector& operator=(Other& other) noexcept
        {
            assert(size() == other.size());
            std::copy(other.begin(), other.end(), m_out.begin());
            return *this;
        }

        template <typename SZ = size_type>
        static typename std::enable_if<
                EmbeddedOutputVector_impl::has_static_size<VV>::value &&
                        !EmbeddedOutputVector_impl::has_constexpr_size<
                                VV>::value,
                SZ>::type
        size() noexcept
        {
            return VV::size();
        }
        template <typename SZ = size_type, typename = void>
        static constexpr typename std::enable_if<
                EmbeddedOutputVector_impl::has_static_size<VV>::value &&
                        !EmbeddedOutputVector_impl::has_constexpr_size<
                                VV>::value,
                SZ>::type
        size() noexcept
        {
            return VV::size();
        }
        template <typename SZ = size_type, typename = void, typename = void>
        typename std::enable_if<
                !EmbeddedOutputVector_impl::has_static_size<VV>::value,
                SZ>::type
        size() const noexcept
        {
            return m_out.size();
        }

        iterator begin() noexcept(noexcept(m_out.begin()))
        {
            return m_out.begin();
        }
        const_iterator begin() const noexcept(noexcept(m_out.begin()))
        {
            return m_out.begin();
        }
        const_iterator cbegin() const noexcept(noexcept(begin()))
        {
            return begin();
        }
        iterator end() noexcept(noexcept(m_out.end())) { return m_out.end(); }
        const_iterator end() const noexcept(noexcept(m_out.end()))
        {
            return m_out.end();
        }
        const_iterator cend() const noexcept(noexcept(end()))
        {
            return end();
        }

        reference operator[](size_type idx) noexcept(noexcept(m_out[0]))
        {
            return m_out[idx];
        }
        const_reference operator[](size_type idx) const
                noexcept(noexcept(m_out[0]))
        {
            return m_out[idx];
        }

        template <typename PTR = pointer>
        typename std::enable_if<
                impl::is_contiguous<
                        typename std::remove_reference<VV>::type>::value,
                PTR>::type
        data() noexcept
        {
            return begin();
        }

        template <typename CPTR = const_pointer>
        typename std::enable_if<
                impl::is_contiguous<
                        typename std::remove_reference<VV>::type>::value,
                CPTR>::type
        data() const noexcept
        {
            return begin();
        }

        template <typename IN>
        EmbeddedOutputVector& operator<<(const IN& in) noexcept
        {
            m_parent(m_out, in);
            return *this;
        }

        template <typename IN>
        friend EmbeddedOutputVector&
        operator>>(const IN& in, EmbeddedOutputVector& out) noexcept
        {
            return out << in;
        }
    };

    template <typename BASE, typename FLT, std::size_t NOUT>
    class OutputEmbedder {
    private:
        template <typename V, std::size_t... IDXS>
        constexpr static V make_out(cppcompat::index_sequence<IDXS...> /* unused */)
        {
            return V{{(static_cast<void>(IDXS),
                       static_cast<typename V::value_type>(0))...}};
        }

    public:
        template <typename V>
        EmbeddedOutputVector<V&, const BASE&> evaluator(V& out) const
                noexcept(noexcept(EmbeddedOutputVector<V&, const BASE&>(
                        std::declval<const BASE&>(), out)))
        {
            return {static_cast<const BASE&>(*this), out};
        }
        template <typename V = std::array<FLT, NOUT>>
        EmbeddedOutputVector<V, const BASE&> evaluator(
                V&& out = make_out<V>(cppcompat::make_index_sequence<NOUT>())) const
                noexcept(noexcept(EmbeddedOutputVector<V, const BASE&>(
                        std::declval<const BASE&>(), std::move(out))))
        {
            return {static_cast<const BASE&>(*this), std::move(out)};
        }
    };
} // namespace simpleNN

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
