/** @file include/simplenn/simplenn.h
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2021-01-28
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include "simplenn/simplenn-config.h"

#include "simplenn/cppcompat.h"
#include "simplenn/Dense.h"
#include "simplenn/EmbeddedOutputVector.h"
#include "simplenn/Embedding.h"
#include "simplenn/GRU.h"
#include "simplenn/processing.h"
#include "simplenn/scattergather.h"
#include "simplenn/span.h"
#include "simplenn/utils.h"

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
