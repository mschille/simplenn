/** @file include/simplenn/utils.h
 *
 * @brief various utility functions
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2019-08-08
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include <cmath>
#include <type_traits>

#include "cppcompat.h"

namespace simpleNN {
    /// implementation details
    namespace impl {
        template <typename T, typename = void>
        struct is_contiguous : std::false_type {};
        template <typename T>
        struct is_contiguous<
                T, cppcompat::void_t<decltype(std::declval<const T&>().data())>>
                : std::true_type {};
        template <typename T>
        struct is_contiguous<T[], void> : std::true_type {};
        template <typename T>
        struct is_contiguous<const T[], void> : std::true_type {};
        template <typename T>
        struct is_contiguous<volatile T[], void> : std::true_type {};
        template <typename T>
        struct is_contiguous<const volatile T[], void> : std::true_type {};
        template <typename T>
        struct is_contiguous<T*, void> : std::true_type {};
        template <typename T>
        struct is_contiguous<const T*, void> : std::true_type {};
        template <typename T>
        struct is_contiguous<volatile T*, void> : std::true_type {};
        template <typename T>
        struct is_contiguous<const volatile T*, void> : std::true_type {};

        template <typename T>
        constexpr T min(T x) noexcept
        {
            return x;
        }
        template <typename T>
        constexpr T min(T x, T y) noexcept
        {
            return (x < y) ? x : y;
        }
        template <
                typename T, typename... Ts,
                typename = typename std::enable_if<(1 < sizeof...(Ts))>::type>
        constexpr T min(T x, Ts&&... args) noexcept
        {
            return min(x, min(std::forward<Ts>(args)...));
        }
        template <typename T>
        constexpr T max(T x) noexcept
        {
            return x;
        }
        template <typename T>
        constexpr T max(T x, T y) noexcept
        {
            return (y < x) ? x : y;
        }
        template <
                typename T, typename... Ts,
                typename = typename std::enable_if<(1 < sizeof...(Ts))>::type>
        constexpr T max(T x, Ts&&... args) noexcept
        {
            return max(x, max(std::forward<Ts>(args)...));
        }

        static_assert(min(1) == 1, "min implementation error");
        static_assert(min(2, 1) == 1, "min implementation error");
        static_assert(min(3, 2, 1) == 1, "min implementation error");
        static_assert(max(1) == 1, "max implementation error");
        static_assert(max(1, 2) == 2, "max implementation error");
        static_assert(max(1, 2, 3) == 3, "max implementation error");

        template <typename... ARGS>
        constexpr int nop(ARGS&&... /* unused */) noexcept
        {
            return 0;
        }

        constexpr bool all() noexcept { return true; }
        constexpr bool any() noexcept { return false; }
        template <typename T, typename... Ts>
        constexpr bool all(T&& val, Ts&&... vals) noexcept
        {
            return bool(std::forward<T>(val)) &&
                   all(std::forward<Ts>(vals)...);
        }
        template <typename T, typename... Ts>
        constexpr bool any(T&& val, Ts&&... vals) noexcept
        {
            return bool(std::forward<T>(val)) ||
                   any(std::forward<Ts>(vals)...);
        }
    } // namespace impl
} //namespace simpleNN

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
