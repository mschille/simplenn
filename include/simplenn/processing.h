/** @file include/simplenn/processing.h
 *
 * @brief various processing helpers for use in neural nets
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @author Vukan Jevtic <vukan.jevtic@cern.ch>
 * @date 2019-08-08
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <limits>
#include <numeric>
#include <type_traits>
#include <vector>

#include "utils.h"

#if !defined(SIMPLENN_NOLIBFVM)
#include "libfvm/fvm.h"
#endif // !defined(SIMPLENN_NOLIBFVM)

namespace simpleNN {
    namespace procs {
        namespace math {
#if defined(SIMPLENN_NOLIBFVM)
            using std::abs;
            using std::exp;
            using std::log;
            using std::max;
            using std::min;
            using std::tanh;
            template <typename T>
            constexpr inline T sel(const bool cond, const T a,
                                   const T b) noexcept
            {
                return cond ? a : b;
            }
            template <typename U, typename V, typename W,
                      typename T = typename std::common_type<
                              typename std::common_type<U, V>::type, W>::type>
            constexpr inline
                    typename std::enable_if<std::is_floating_point<T>::value,
                                            T>::type
                    fma(const U x, const V y, const W z) noexcept
            {
                return x * y + z;
            }
#else // NOLIBFVM
            using fvm::abs;
            using fvm::exp;
            using fvm::fma;
            using fvm::log;
            using fvm::max;
            using fvm::min;
            using fvm::sel;
            using fvm::tanh;
#endif // NOLIBFVM
        } // namespace math
        /// identity
        struct id {
            template <typename T>
            T operator()(const T val) const noexcept
            {
                return val;
            }
        };
        /// linear transformation
        template <typename FLT = float>
        class linear {
        private:
            FLT m_a, m_b;

        public:
            explicit linear(FLT a, FLT b) : m_a(a), m_b(b) {}
            FLT operator()(FLT val) const noexcept
            {
                return math::fma(m_a, val, m_b);
            }
        };
        /// minimum
        template <typename FLT = float>
        class min {
        private:
            FLT m_min;

        public:
            explicit min(FLT min) : m_min(min) {}
            FLT operator()(FLT val) const noexcept
            {
                return math::min(val, m_min);
            }
        };
        /// maximum
        template <typename FLT = float>
        class max {
        private:
            FLT m_max;

        public:
            explicit max(FLT max) : m_max(max) {}
            FLT operator()(FLT val) const noexcept
            {
                return math::max(val, m_max);
            }
        };
        /// range
        template <typename FLT = float>
        class range {
        private:
            FLT m_min;
            FLT m_max;

        public:
            constexpr explicit range(FLT min, FLT max)
                    : m_min((assert(min < max), min)), m_max(max)
            {}
            FLT operator()(FLT val) const noexcept
            {
                return math::max(m_min, math::min(val, m_max));
            }
        };
        struct sigmoid {
            template <typename FLT>
            FLT operator()(FLT val) const noexcept
            {
                return 1 / (1 + math::exp(-val));
            }
        };

        struct hard_sigmoid {
            template <typename FLT>
            FLT operator()(FLT val) const noexcept
            {
                return math::max(
                        FLT(0),
                        math::min(FLT(1), math::fma(FLT(1) / FLT(5), val,
                                                    FLT(1) / FLT(2))));
            }
        };
        
        struct exponential {
            template <typename FLT>
            FLT operator()(FLT val) const noexcept
            {
                return math::exp(val);
            }
        };

        struct tanh {
            template <typename FLT>
            FLT operator()(FLT val) const noexcept
            {
                return math::tanh(val);
            }
        };
        
        struct ReLU {
            template <typename FLT>
            FLT operator()(FLT val) const noexcept
            {
                return math::max<FLT>(0, val);
            }
        };

        template <typename FLT>
        class LeakyReLU {
            private:
                const FLT alpha;
            public:
                explicit LeakyReLU(FLT a) : alpha(a) { }
                FLT operator()(FLT val) const noexcept
                {
                    return math::sel(val < 0, alpha*val, val);
                }
        };
        
        struct softsign {
            template <typename FLT>
            FLT operator()(FLT val) const noexcept
            {
                return val / (math::abs(val) + 1);
            }
        };
        
        struct softplus {
            template <typename FLT>
            FLT operator()(FLT val) const noexcept
            {
                return math::log(math::exp(val) + 1);
            }
        };
        
        template <typename FLT>
        class elu {
            private:
                const FLT alpha = 1;
            public:
                elu() = default;
                explicit elu(FLT a) : alpha(a) { }
                
                FLT operator()(FLT val) const noexcept
                {
                    return math::sel(val < 0, (alpha * (math::exp(val) - 1)), val);
                }
        };
        
        class selu {
            public:
                template <typename FLT>
                FLT operator()(FLT val) const noexcept
                {
                    constexpr FLT alpha = FLT(1.6732632423543772848170429916717);
                    constexpr FLT scale = FLT(1.0507009873554804934193349852946);
                    return scale * math::sel(val < 0, alpha * (math::exp(val) - 1), val);
                }
        };

        /// matrix cruncher: calculates out += M * in
        template <std::size_t NOUT, std::size_t NIN, typename FLT = float>
        class matrix_cruncher {
        private:
            std::vector<FLT> m_mat;

            /// helper for alignment: integer base 2 logarithm (constexpr)
            template <typename T, typename = typename std::enable_if<std::is_unsigned<T>::value>::type>
            constexpr static unsigned log2(const T i) noexcept
            {
                if (!i) return 0u;
                unsigned retVal = 0;
                T j = 1;
                while (i > j && j) {
                    ++retVal;
                    j <<= 1;
                }
                return retVal;
            }
            static_assert(log2(0x0ul) == 0u, "log2");
            static_assert(log2(0x1ul) == 0u, "log2");
            static_assert(log2(0x2ul) == 1u, "log2");
            static_assert(log2(0x3ul) == 2u, "log2");
            static_assert(log2(0x4ul) == 2u, "log2");
            static_assert(log2(0x5ul) == 3u, "log2");
            static_assert(log2(0x7ul) == 3u, "log2");
            static_assert(log2(0x8ul) == 3u, "log2");
            static_assert(log2(0x9ul) == 4u, "log2");
            static_assert(log2(0xful) == 4u, "log2");
            static_assert(log2(0x80000000ul) == 31u, "log2");
            static_assert(log2(0x80000001ul) == 32u, "log2");
            static_assert(log2(0xfffffffful) == 32u, "log2");

            // calculate aligned input size: size of vector in bytes rounded
            // up to next power of two or 64 bytes (which is assumed to be the
            // size of a cache line), whichever is smaller
            //
            // the rationale here is to be reasonably friendly to platforms
            // which need a particular alignment for maximum speed (when this
            // code gets autovectorised), while preserving space in the cache
            // if we can (i.e. we don't need cacheline-aligned rows if we can
            // fit two rows in a cacheline without breaking alignment
            // constraints).
            constexpr static std::size_t AL_BYTES = std::min(
                    std::size_t(1) << log2(sizeof(FLT) * NIN), size_t(64));
            constexpr static std::size_t NIN_AL =
                    (AL_BYTES *
                     ((NIN * sizeof(FLT) + (AL_BYTES - 1)) / AL_BYTES)) /
                    sizeof(FLT);
            static_assert(NIN <= NIN_AL, "error in calculation of number of "
                                         "aligned input elements");

            void fix_row_alignment()
            {
                if (NIN == NIN_AL) return;
                m_mat.reserve(NOUT * NIN_AL);
                for (auto it = m_mat.begin(); m_mat.end() != it; ) {
                    // pad with sufficient number of zeros, so we can SIMD
                    // away on the padded elements
                    it = m_mat.insert(it + NIN, NIN_AL - NIN, FLT(0)) +
                         (NIN_AL - NIN);
                }
            }

        public:
            template <typename IT>
            explicit matrix_cruncher(IT first, IT last)
                    : m_mat((assert(std::distance(first, last) == NOUT * NIN),
                             first),
                            last)
            { fix_row_alignment(); }
            explicit matrix_cruncher(const std::initializer_list<FLT> ilist)
                    : matrix_cruncher(ilist.begin(), ilist.end())
            {}
	    explicit matrix_cruncher(const std::vector<FLT>& m) :
		matrix_cruncher(m.begin(), m.end())
	    {}
	    explicit matrix_cruncher(std::vector<FLT>&& m) :
		m_mat((assert(m.size() == NOUT * NIN), std::move(m)))
	    { fix_row_alignment(); }

        private:
            // doIt_aligned below is where it all happens; since performance is
            // critical in this number crunching bit of code, we hint GCC and
            // clang to do their very best, and give the compiler some freedom
            // to optimise things we would normally not permit (we assume that
            // floating poing addition is associative, allowing adds to be done
            // in a different order)
#if !defined(SIMPLENN_NOVECTORIZE)
#if defined(__GNUC__) && !defined(__clang__)
            // gcc - tweak optimisation settings for a single function
#pragma GCC push_options
#pragma GCC optimize("-Ofast", "-ffast-math")
#endif
#endif
            template <typename OUT>
            // force all calls inlined, try hard to optimise
            [[gnu::flatten, gnu::hot]]
            static inline void doIt_aligned(OUT& out, const FLT* mat, const FLT* in) noexcept
            {
                for (std::size_t i = 0; NOUT != i; ++i) {
                    FLT sum = 0;
#if !defined(SIMPLENN_NOVECTORIZE)
#if defined(__clang__)
                    // clang - hint to optimise heck out of the next loop
#pragma clang loop vectorize(enable)
#pragma clang loop interleave(enable)
#endif
#endif
                    for (std::size_t k = 0; NIN_AL != k; ++k) {
                        // no FMA here to enable vectorisation
                        sum += mat[NIN_AL * i + k] * in[k];
                    }
                    out[i] += sum;
                }
            }
#if !defined(SIMPLENN_NOVECTORIZE)
#if defined(__GNUC__) && !defined(__clang__)
            // gcc - turn optimisation tweaks off
#pragma GCC pop_options
#endif
#endif

            template <typename OUT, typename IN>
            void doIt_unaligned(OUT& out, const IN& in) const noexcept
            {
                // get a cache-line aligned copy of in
                alignas(64) std::array<FLT, NIN_AL> tmp;
                std::fill(std::copy(in.begin(), in.end(), tmp.begin()), tmp.end(), FLT(0));
                return doIt_aligned(out, m_mat.data(), tmp.data());
            }

        public:
            template <typename OUTTYPE, typename INTYPE>
            typename std::enable_if<impl::is_contiguous<INTYPE>::value>::type
            operator()(OUTTYPE& out, const INTYPE& in) const noexcept
            {
                return (assert(NOUT == out.size()), assert(NIN == in.size()),
                        (((reinterpret_cast<std::size_t>(in.data()) & 63) || (0 != NIN % NIN_AL))
                                 ? doIt_unaligned(out, in)
                                 : doIt_aligned(out, m_mat.data(), in.data())));
            }

            template <typename OUTTYPE, typename INTYPE>
            typename std::enable_if<!impl::is_contiguous<INTYPE>::value>::type
            operator()(OUTTYPE& out, const INTYPE& in) const noexcept
            {
                return (assert(NOUT == out.size()), assert(NIN == in.size()),
                        doIt_unaligned(out, in));
            }
        };

        /// tag by which to recognise vector pre-/post-processing
        template <std::size_t SZ>
        struct vector_proc_base {
            constexpr static std::size_t size() noexcept { return SZ; }
        };

        /// softmax
        template <std::size_t SZ>
        struct softmax : vector_proc_base<SZ> {
            template <typename OUTVEC, typename VEC>
            void operator()(OUTVEC& out, const VEC& in) const noexcept
            {
                using std::begin;
                using std::end;
                using FLT = typename std::remove_cv<
                        typename std::remove_reference<decltype(
                                out[0])>::type>::type;
                assert(SZ == in.size());
                assert(out.size() == in.size());
                for (std::size_t i = 0; SZ != i; ++i) {
                    out[i] = math::exp(in[i]);
                }
                const FLT isum =
                        FLT(1) / static_cast<FLT>(std::accumulate(
                                         begin(out), end(out), FLT(0)));
                for (std::size_t i = 0; SZ != i; ++i) {
                    out[i] *= isum;
                }
            }

            template <typename VEC,
                      typename FLT = typename std::remove_cv<
                              typename std::remove_reference<decltype(
                                      std::declval<const VEC&>()[0])>::type>::
                              type>
            std::array<FLT, SZ> operator()(const VEC& in) const noexcept
            {
                std::array<FLT, SZ> retVal;
                operator()(retVal, in);
                return retVal;
            }
        };
    } // namespace procs
} // namespace simpleNN

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
