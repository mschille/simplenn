/** @file include/simplenn/Dense.h
 *
 * @brief a dense NN layer
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2019-08-08
 *
 * For copyright and license information, see the end of the file.
 */

#pragma once

#include <tuple>
#include <vector>
#include <cassert>
#include <utility>
#include <algorithm>

#include "cppcompat.h"
#include "processing.h"
#include "EmbeddedOutputVector.h"

namespace simpleNN {
    namespace impl {
        template <typename T>
        struct procsize : std::integral_constant<std::size_t, T::size()> {};
        // specialise for tuples
        template <typename... Ts>
        struct procsize<std::tuple<Ts...>>
                : std::integral_constant<std::size_t, sizeof...(Ts)> {};
        // and for arrays
        template <typename T, std::size_t SZ>
        struct procsize<std::array<T, SZ>>
                : std::integral_constant<std::size_t, SZ> {};
    } // namespace impl
    template <typename POSTPROCTUPLE, typename PREPROCTUPLE,
              typename FLT = float>
    class Dense
            : public OutputEmbedder<Dense<POSTPROCTUPLE, PREPROCTUPLE, FLT>,
                                    FLT,
                                    impl::procsize<POSTPROCTUPLE>::value> {
    private:
        static constexpr auto INSZ = impl::procsize<PREPROCTUPLE>::value;
        static constexpr auto OUTSZ = impl::procsize<POSTPROCTUPLE>::value;
        POSTPROCTUPLE m_postprocs;
        PREPROCTUPLE m_preprocs;
        std::vector<FLT> m_b;
        procs::matrix_cruncher<OUTSZ, INSZ, FLT> m_cruncher;

        template <typename INTYPE, std::size_t... IDXIN, typename... PREPROCS>
        static std::array<FLT, INSZ>
        preproc(cppcompat::index_sequence<IDXIN...> /* unused */,
                const std::tuple<PREPROCS...>& preprocs,
                const INTYPE& in) noexcept
        {
            return {{ std::get<IDXIN>(preprocs)(in[IDXIN])... }};
        }

        template <typename INTYPE, std::size_t... IDXIN, typename PREPROC,
                  std::size_t PPSZ>
        static std::array<FLT, INSZ>
        preproc(cppcompat::index_sequence<IDXIN...> /* unused */,
                const std::array<PREPROC, PPSZ>& preprocs,
                const INTYPE& in) noexcept
        {
            return {{ preprocs[IDXIN](in[IDXIN])... }};
        }

        template <typename OUTTYPE, std::size_t... IDXOUT,
                  typename... POSTPROCS>
        static void postproc(cppcompat::index_sequence<IDXOUT...> /* unused */,
                             const std::tuple<POSTPROCS...>& postprocs,
                             OUTTYPE& out) noexcept
        {
            impl::nop((out[IDXOUT] = std::get<IDXOUT>(postprocs)(out[IDXOUT]),
                       0)...);
        }

        template <typename OUTTYPE, std::size_t... IDXOUT,
                  typename POSTPROC, std::size_t PPSZ>
        static void postproc(cppcompat::index_sequence<IDXOUT...> /* unused */,
                             const std::array<POSTPROC, PPSZ>& postprocs,
                             OUTTYPE& out) noexcept
        {
            impl::nop((out[IDXOUT] = postprocs[IDXOUT](out[IDXOUT]),
                       0)...);
        }

        template <typename T, typename INTYPE, std::size_t... IDXIN,
                  typename = typename std::enable_if<std::is_base_of<
                          procs::vector_proc_base<INSZ>, T>::value>::type>
        static std::array<FLT, INSZ>
        preproc(cppcompat::index_sequence<IDXIN...> /* unused */, const T& preprocs,
                const INTYPE& in) noexcept
        {
            return preprocs(in);
        }

        template <typename T, typename OUTTYPE, std::size_t... IDXOUT,
                  typename = typename std::enable_if<std::is_base_of<
                          procs::vector_proc_base<OUTSZ>, T>::value>::type>
        static void postproc(cppcompat::index_sequence<IDXOUT...> /* unused */,
                             const T& postprocs, OUTTYPE& out) noexcept
        {
            postprocs(out, out);
        }

    public:
        template <typename ITB, typename ITM>
        Dense(POSTPROCTUPLE&& postprocs, PREPROCTUPLE&& preprocs, ITB bfirst,
              ITB blast, ITM mfirst, ITM mlast)
                : m_postprocs(std::forward<POSTPROCTUPLE>(postprocs)),
                  m_preprocs(std::forward<PREPROCTUPLE>(preprocs)),
                  m_b((assert(OUTSZ == std::distance(bfirst, blast)), bfirst),
                      blast),
                  m_cruncher(mfirst, mlast)
        {}

        template <typename FLT2>
        Dense(POSTPROCTUPLE&& postprocs, PREPROCTUPLE&& preprocs,
               std::initializer_list<FLT2> b, std::initializer_list<FLT2> m)
                : Dense(std::forward<POSTPROCTUPLE>(postprocs),
                         std::forward<PREPROCTUPLE>(preprocs), b.begin(),
                         b.end(), m.begin(), m.end())
        {}
        Dense(POSTPROCTUPLE&& postprocs, PREPROCTUPLE&& preprocs,
              const std::vector<FLT>& b, const std::vector<FLT>& m)
                : Dense(std::forward<POSTPROCTUPLE>(postprocs),
                        std::forward<PREPROCTUPLE>(preprocs), b.begin(),
                        b.end(), m.begin(), m.end())
        {}
        Dense(POSTPROCTUPLE&& postprocs, PREPROCTUPLE&& preprocs,
              std::vector<FLT>&& b, std::vector<FLT>&& m)
                : m_postprocs(std::forward<POSTPROCTUPLE>(postprocs)),
                  m_preprocs(std::forward<PREPROCTUPLE>(preprocs)),
                  m_b((assert(OUTSZ == b.size()), std::move(b))),
                  m_cruncher(std::move(m))
        {}

        template <typename OUTTYPE, typename INTYPE>
        void operator()(OUTTYPE& out, const INTYPE& in) const noexcept
        {
            // verify shape of arguments
            assert(INSZ == in.size());
            assert(OUTSZ == out.size());
            // preproces
            alignas(64) const std::array<FLT, INSZ> inpreproc =
                    Dense::preproc(cppcompat::make_index_sequence<INSZ>(), m_preprocs, in);
            // do the hard bit
            std::copy(m_b.begin(), m_b.end(), out.begin());
            m_cruncher(out, inpreproc);
            // postprocess (in-place)
            Dense::postproc(cppcompat::make_index_sequence<OUTSZ>(), m_postprocs, out);
        }
    };

    template <typename FLT = float, typename POSTPROCTUPLE,
              typename PREPROCTUPLE, typename ITB, typename ITM>
    Dense<POSTPROCTUPLE, PREPROCTUPLE, FLT>
    make_dense(POSTPROCTUPLE&& postprocs, PREPROCTUPLE&& preprocs,
                ITB bfirst, ITB blast, ITM mfirst, ITM mlast)
    {
        return {std::forward<POSTPROCTUPLE>(postprocs),
                std::forward<PREPROCTUPLE>(preprocs),
                bfirst,
                blast,
                mfirst,
                mlast};
    }

    template <typename FLT = float, typename POSTPROCTUPLE,
              typename PREPROCTUPLE, typename FLT2>
    Dense<POSTPROCTUPLE, PREPROCTUPLE, FLT>
    make_dense(POSTPROCTUPLE&& postprocs, PREPROCTUPLE&& preprocs,
                std::initializer_list<FLT2> b, std::initializer_list<FLT2> m)
    {
        return {std::forward<POSTPROCTUPLE>(postprocs),
                std::forward<PREPROCTUPLE>(preprocs),
                b.begin(),
                b.end(),
                m.begin(),
                m.end()};
    }
} // namespace simpleNN

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
