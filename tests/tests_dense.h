/** @file tests_dense.h
 *
 * @brief unit tests for dense NN layer
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2019-08-08
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include <array>
#include <limits>

#include "Test.h"

#include "simplenn/processing.h"
#include "simplenn/Dense.h"

TestStats test_dense()
{
    Test t("dense");
    using namespace simpleNN;
    static auto node1 = make_dense(
            std::make_tuple(procs::linear<float>(0.1f, 0.9f),
                            procs::min<float>(0.3f)),
            std::make_tuple(procs::linear<float>(.1f, .2f),
                            procs::linear<float>(.2f, .3f), procs::id()),
            {0, 0}, {0, 1, 2, 1, 2, 3});
    static auto node2 = make_dense(std::make_tuple(procs::id(), procs::id()),
                                   std::make_tuple(procs::id(), procs::id()),
                                   {0, 0}, {0, 1, 1, 0});
    const std::array<float, 3> in{{ 2.2f, 0.4f, -1.2f }};
    std::array<float, 2> out;
    auto eval1 = node1.evaluator();
    auto eval2 = node2.evaluator(out);
    in >> eval1 >> eval2;
    t.checkEQ(eval2.size(), 2u);
    t.checkLT(std::abs(eval2[0] - -2.42f),
            2.42f * 4.f * std::numeric_limits<float>::epsilon());
    t.checkLT(std::abs(eval2[1] - 0.698f),
            .698f * 4.f * std::numeric_limits<float>::epsilon());
    // Testing softmax
    static auto node3 = make_dense(procs::softmax<2>(), procs::softmax<3>(),
            {0, 0}, {1, 1, 1, 1, -1, 1});
    auto eval3 = node3.evaluator();
    in >> eval3;
    t.checkLT(std::abs(eval3[0] - .568517f),
            .568517f * 4.f * std::numeric_limits<float>::epsilon());
    t.checkLT(std::abs(eval3[1] - .4314829f),
            .4314829f * 4.f * std::numeric_limits<float>::epsilon());
    // Testing selu
    std::array<float, 3> out2;
    static auto node4 = make_dense(std::make_tuple(procs::selu(), procs::selu(), procs::selu()),
                                   std::make_tuple(procs::id(), procs::id(), procs::id()),
                                   {0, 0, 0}, {1, 0, 0, 0, 1, 0, 0, 0, 1});
    auto eval4 = node4.evaluator(out2);
    in >> eval4;
    t.checkLT(std::abs(out2[0] -  2.311542172f), 2.311542172f * 4.f * std::numeric_limits<float>::epsilon());
    t.checkLT(std::abs(out2[1] -  0.420280395f), 0.420280395f * 4.f * std::numeric_limits<float>::epsilon());
    t.checkLT(std::abs(out2[2] - -1.228569995f), 1.228569995f * 4.f * std::numeric_limits<float>::epsilon());

    // Testing elu
    static auto node5 = make_dense(std::make_tuple(procs::elu<float>(), procs::elu<float>(2), procs::elu<float>(3)),
                                   std::make_tuple(procs::id(), procs::id(), procs::id()),
                                   {0, 0, 0}, {1, 0, 0, 0, 1, 0, 0, 0, 1});
    auto eval5 = node5.evaluator(out2);
    in >> eval5;
    t.checkLE(std::abs(out2[0] - 2.2f), 2.2f * 4.f * std::numeric_limits<float>::epsilon());
    t.checkLE(std::abs(out2[1] - 0.4f), 0.4f * 4.f * std::numeric_limits<float>::epsilon());
    t.checkLT(std::abs(out2[2] - -2.096417364f), 2.096417364f * 4.f * std::numeric_limits<float>::epsilon());

    return t;
}

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
