
/** @file tests_embedding.h
 *
 * @brief unit tests for embedding layer
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-03-16
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include <array>
#include <limits>

#include "Test.h"

#include "simplenn/Embedding.h"

TestStats test_embedding()
{
    Test t("embedding");
    using namespace simpleNN;
    static Embedding<3, 1, 3, 3> node1({
            0.f, 1.f, 2.f, 3.f, 4.f, 5.f, 6.f, 7.f, 8.f});
    static Embedding<9, 3, 3, 3> node2({
            0.f, 1.f, 2.f, 3.f, 4.f, 5.f, 6.f, 7.f, 8.f});
    unsigned in = 0;
    auto eval1 = node1.evaluator();
    std::array<float, 3> out;
    auto eval2 = node1.evaluator(out);
    in >> eval1;
    t.checkEQ(3u, eval1.size());
    t.checkEQ(0.f, eval1[0]);
    t.checkEQ(1.f, eval1[1]);
    t.checkEQ(2.f, eval1[2]);
    in = 1;
    in >> eval2;
    t.checkEQ(3u, eval2.size());
    t.checkEQ(3.f, out[0]);
    t.checkEQ(4.f, out[1]);
    t.checkEQ(5.f, out[2]);
    const std::array<unsigned, 3> in2 = {{ 0, 1, 2 }};
    auto eval3 = node2.evaluator();
    in2 >> eval3;
    t.checkEQ(9u, eval3.size());
    for (unsigned i = 0; 9 != i; ++i) t.checkEQ(float(i), eval3[i]);

    return t;
}

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
