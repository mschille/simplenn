/** @file tests_span.h
 *
 * @brief unit tests for span functionality
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2019-08-08
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include <vector>
#include <array>

#include "simplenn/span.h"

#include "Test.h"

TestStats test_const_span_array()
{
    Test t("const_span array");

    using namespace simpleNN;

    const std::array<int, 4> arr{{ 0, 1, 2, 3 }};

    auto s = make_span(arr, 1, 3);
    t.check(s.size() == 2);
    t.check(s.data() == &arr[1]);
    t.check(s[0] == 1);
    t.check(s[1] == 2);
    {
        unsigned i = 0;
        for (auto el : s) { t.check(el == arr[1 + i++]); }
    }

    return t;
}

TestStats test_const_span_vector()
{
    Test t("const_span vector");

    using namespace simpleNN;

    const std::vector<int> arr{{ 0, 1, 2, 3 }};

    auto s = make_span(arr, 1, 3);
    t.check(s.size() == 2);
    t.check(s.data() == &arr[1]);
    t.check(s[0] == 1);
    t.check(s[1] == 2);
    {
        unsigned i = 0;
        for (auto el : s) { t.check(el == arr[1 + i++]); }
    }

    return t;
}

TestStats test_span_array()
{
    Test t("span array");

    using namespace simpleNN;

    std::array<int, 4> arr{{ 0, 0, 0, 0 }};
    auto s = make_span(arr, 1, 3);
    t.check(s.size() == 2);
    t.check(s.data() == &arr[1]);
    s[0] = 1;
    s[1] = 2;
    t.check(arr[0] == 0);
    t.check(arr[1] == 1);
    t.check(arr[2] == 2);
    t.check(arr[3] == 0);
    {
	unsigned i = 0;
	for (auto el: s) { t.check(el == arr[1 + i++]); }
    }

    return t;
}

TestStats test_span_vector()
{
    Test t("span vector");

    using namespace simpleNN;

    std::vector<int> arr{{ 0, 0, 0, 0 }};
    auto s = make_span(arr, 1, 3);
    t.check(s.size() == 2);
    t.check(s.data() == &arr[1]);
    s[0] = 1;
    s[1] = 2;
    t.check(arr[0] == 0);
    t.check(arr[1] == 1);
    t.check(arr[2] == 2);
    t.check(arr[3] == 0);
    {
	unsigned i = 0;
	for (auto el: s) { t.check(el == arr[1 + i++]); }
    }

    return t;
}

TestStats test_const_span_array_compiletime_length()
{
    Test t("const_span array (compiletime length)");

    using namespace simpleNN;

    const std::array<int, 4> arr{{ 0, 1, 2, 3 }};

    auto s = make_span<1, 3>(arr);
    t.check(s.size() == 2);
    t.check(s.data() == &arr[1]);
    t.check(s[0] == 1);
    t.check(s[1] == 2);
    {
        unsigned i = 0;
        for (auto el : s) { t.check(el == arr[1 + i++]); }
    }

    return t;
}

TestStats test_const_span_vector_compiletime_length()
{
    Test t("const_span vector (compiletime length)");

    using namespace simpleNN;

    const std::vector<int> arr{{ 0, 1, 2, 3 }};

    auto s = make_span<1, 3>(arr);
    t.check(s.size() == 2);
    t.check(s.data() == &arr[1]);
    t.check(s[0] == 1);
    t.check(s[1] == 2);
    {
        unsigned i = 0;
        for (auto el : s) { t.check(el == arr[1 + i++]); }
    }

    return t;
}

TestStats test_span_array_compiletime_length()
{
    Test t("span array (compiletime length)");

    using namespace simpleNN;

    std::array<int, 4> arr{{ 0, 0, 0, 0 }};
    auto s = make_span<1, 3>(arr);
    t.check(s.size() == 2);
    t.check(s.data() == &arr[1]);
    s[0] = 1;
    s[1] = 2;
    t.check(arr[0] == 0);
    t.check(arr[1] == 1);
    t.check(arr[2] == 2);
    t.check(arr[3] == 0);
    {
	unsigned i = 0;
	for (auto el: s) { t.check(el == arr[1 + i++]); }
    }

    return t;
}

TestStats test_span_vector_compiletime_length()
{
    Test t("span vector (compiletime length)");

    using namespace simpleNN;

    std::vector<int> arr{{ 0, 0, 0, 0 }};
    auto s = make_span<1, 3>(arr);
    t.check(s.size() == 2);
    t.check(s.data() == &arr[1]);
    s[0] = 1;
    s[1] = 2;
    t.check(arr[0] == 0);
    t.check(arr[1] == 1);
    t.check(arr[2] == 2);
    t.check(arr[3] == 0);
    {
	unsigned i = 0;
	for (auto el: s) { t.check(el == arr[1 + i++]); }
    }

    return t;
}


/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
