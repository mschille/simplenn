/** @file unittests.cc
 *
 * @brief run unit tests
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2019-08-08
 *
 * For copyright and license information, see the end of the file.
 */
#include <iostream>

#include "Test.cc"

#include "tests_scattergather.h"
#include "tests_span.h"
#include "tests_dense.h"
#include "tests_gru.h"
#include "tests_gru_v1.h"
#include "tests_gru_v2.h"
#include "tests_embedding.h"

int main(int /* argc */, char* /* argv */ [])
{
    using Func = TestStats (*)();
    Func tests[] = {
        test_gather_array, test_gather_vector,
        test_scatter_array, test_scatter_vector,
        test_const_span_array, test_const_span_vector,
        test_span_array, test_span_vector,
        test_const_span_array_compiletime_length, test_const_span_vector_compiletime_length,
        test_span_array_compiletime_length, test_span_vector_compiletime_length,
        test_dense, test_gru, test_gru_v1, test_gru_v2, test_embedding
    };

    TestStats stats;
    for (auto test: tests) stats += test();

    if (stats.m_nfail) {
        std::cout << "\nSummary: " << stats.m_nfail << "/"
                  << (stats.m_nfail + stats.m_npass) << " ("
                  << (100.f * float(stats.m_nfail) / float(stats.m_npass + stats.m_nfail))
                  << "%) failed." << std::endl;
    } else {
        std::cout << "\nSummary: All tests passed." << std::endl;
    }

    return (0 == stats.m_nfail) ? 0 : 1;
}

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
