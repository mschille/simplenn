/** @file tests_gru.h
 *
 * @brief unit tests for GRU NN layer
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2019-08-08
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include <array>
#include <cmath>
#include <limits>

#include "Test.h"

#include "simplenn/processing.h"
#include "simplenn/GRU.h"

TestStats test_gru()
{
    Test t("GRU");

    using namespace simpleNN;
    static GRU<4, 3, procs::tanh, procs::sigmoid> gru(
            // bz
            {0.5396307110786438, 0.08844500780105591, -0.4972180426120758,
             0.3089647889137268},
            // Uz
            {-0.023693479597568512, 0.47621065378189087,
             -0.026062732562422752, -0.2883251905441284, -0.02302554063498974,
             0.3682078421115875, 0.08971219509840012, 0.4614763855934143,
             0.43155184388160706, -0.44918689131736755, 0.1266588717699051,
             0.009384357370436192, -0.5413647890090942, -0.09530439972877502,
             0.028170980513095856, -0.5197709202766418},
            // Wz
            {0.6123928427696228, 0.5320025086402893, 0.06597703695297241,
             -0.5080301761627197, -0.5593352913856506, 0.20111560821533203,
             -0.4341028034687042, -0.3397471010684967, 0.15117067098617554,
             -0.1270926594734192, -0.6286153793334961, -0.2065417766571045},
            // br
            {-0.681636393070221, 0.4192182421684265, -0.6511297225952148,
             -0.14377957582473755},
            // Ur
            {-0.004264538176357746, -0.09104374796152115,
             -0.09553708136081696, 0.08247571438550949, 0.31729209423065186,
             0.15219180285930634, 0.18150310218334198, 0.05515687167644501,
             0.5818751454353333, 0.01661701686680317, -0.09261275827884674,
             -0.3881826102733612, 0.08055350929498672, 0.47243642807006836,
             -0.20536307990550995, 0.20774979889392853},
            // Wr
            {0.005812346935272217, -0.5317989587783813, 0.12602698802947998,
             -0.02863079309463501, -0.44884544610977173, 0.6231500506401062,
             -0.5052926540374756, -0.22886160016059875, 0.21646791696548462,
             -0.5166099667549133, -0.07095938920974731, -0.16953903436660767},
            // bh
            {-0.16086870431900024, 0.1490958333015442, 0.8027984499931335,
             -0.3756389021873474},
            // Uh
            {-0.008441035635769367, -0.06564085930585861, 0.45082777738571167,
             0.25174325704574585, -0.16629770398139954, -0.027263687923550606,
             0.6930670738220215, 0.04654671251773834, -0.15605369210243225,
             -0.0017893059412017465, -0.2540276348590851, 0.15848593413829803,
             0.14750131964683533, 0.40843087434768677, 0.3653722405433655,
             -0.3732411861419678},
            // Wh
            {-0.5794509053230286, -0.13676726818084717, -0.0425758957862854,
             -0.045345187187194824, 0.45919984579086304, -0.2460642158985138,
             -0.2341310679912567, -0.019682347774505615, 0.4934735894203186,
             -0.5472712516784668, 0.5577864050865173, -0.5832403898239136});

    std::array<float, 3> inputs[] = {
            {{-0.9095454221789239, -0.3493393384734713,
              -0.22264542062103598}},
            {{-0.4573019364522082, 0.6574750183038587, -0.28649334661282144}},
            {{-0.4381309806252385, 0.08539216631649693, -0.7181515500504747}},
            {{0.6043939615080793, -0.8508987126404584, 0.9737738732010346}},
            {{0.5444895385933148, -0.6025686369316552, -0.9889557657527952}},
    };
    const std::array<float, 4> outputs[] = {
            {{0.2216009795665741, 0.02801867015659809, 0.3643605411052704,
              0.019060123711824417}},
            {{0.1735839992761612, 0.2992372512817383, 0.515657901763916,
              0.23914137482643127}},
            {{0.189884752035141, 0.36591047048568726, 0.49146029353141785,
              0.3082898259162903}},
            {{0.0029392093420028687, 0.128476083278656, 0.6760960221290588,
              -0.22018525004386902}},
            {{-0.10044924169778824, 0.1700284779071809, 0.3214035630226135,
              -0.24070334434509277}}};

    auto eval = gru.evaluator();
    auto it = std::begin(outputs);
    for (const auto& features : inputs) {
        eval << features;
        t.checkEQ(eval.size(), 4u);
        {
            const auto& output = *it++;
            for (unsigned i = 0; eval.size() != i; ++i) {
                t.checkLT(std::abs(eval[i] - output[i]),
                        64.f * std::abs(output[i]) *
                                std::numeric_limits<float>::epsilon());
            }
        }
    }
    return t;
}

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
