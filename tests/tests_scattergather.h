/** @file tests_scattergather.h
 *
 * @brief unit tests for scatter/gather functionality
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2019-08-08
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include <vector>
#include <array>

#include "simplenn/scattergather.h"

#include "Test.h"

TestStats test_gather_array()
{
    Test t("gather array");

    using namespace simpleNN;

    const std::array<int, 4> arr{{ 0, 1, 2, 3 }};

    auto g = gather<1, 3>(arr);
    t.checkEQ(g.size(), 2u);
    t.checkEQ(g[0], 1);
    t.checkEQ(g[1], 3);
    {
        unsigned i = 0;
        for (auto el : g) { t.checkEQ(el, arr[2 * i++ + 1]); }
    }

    return t;
}

TestStats test_gather_vector()
{
    Test t("gather vector");

    using namespace simpleNN;

    const std::vector<int> arr{{ 0, 1, 2, 3 }};

    auto g = gather<1, 3>(arr);
    t.checkEQ(g.size(), 2u);
    t.checkEQ(g[0], 1);
    t.checkEQ(g[1], 3);
    {
        unsigned i = 0;
        for (auto el : g) { t.checkEQ(el, arr[2 * i++ + 1]); }
    }

    return t;
}

TestStats test_scatter_array()
{
    Test t("scatter array");

    using namespace simpleNN;

    std::array<int, 4> arr{{ 0, 0, 0, 0 }};
    auto s = scatter<1, 2>(arr);
    t.checkEQ(s.size(), 2u);
    s[0] = 1;
    s[1] = 2;
    t.checkEQ(arr[0], 0);
    t.checkEQ(arr[1], 1);
    t.checkEQ(arr[2], 2);
    t.checkEQ(arr[3], 0);
    {
	unsigned i = 0;
	for (auto el: s) { t.checkEQ(el, arr[1 + i++]); }
    }

    return t;
}

TestStats test_scatter_vector()
{
    Test t("scatter vector");

    using namespace simpleNN;

    std::vector<int> arr{{ 0, 0, 0, 0 }};
    auto s = scatter<1, 2>(arr);
    t.checkEQ(s.size(), 2u);
    s[0] = 1;
    s[1] = 2;
    t.checkEQ(arr[0], 0);
    t.checkEQ(arr[1], 1);
    t.checkEQ(arr[2], 2);
    t.checkEQ(arr[3], 0);
    {
	unsigned i = 0;
	for (auto el: s) { t.checkEQ(el, arr[1 + i++]); }
    }

    return t;
}

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
