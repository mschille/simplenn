/** @file Test.h
 *
 * @brief very simple unit testing framework
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2019-08-08
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include <string>
#include <vector>
#include <utility>
#include <iostream>

struct TestStats {
    unsigned m_npass = 0;
    unsigned m_nfail = 0;
    TestStats& operator+=(const TestStats& other)
    {
        m_npass += other.m_npass;
        m_nfail += other.m_nfail;
        return *this;
    }
};

class Test {
    public:
        struct Location {
            unsigned m_line;
            const char* m_file;
            const char* m_condstr;
        };
    private:
        TestStats m_stats;
        std::vector<Location> m_fails;
        std::string m_name;
    public:
        Test(const char* name);

        ~Test();

        void _check(Location loc, bool cond, bool expect_fail = false)
        {
            if (!cond) {
                if (!expect_fail) ++m_stats.m_nfail;
                m_fails.push_back(std::move(loc));
            } else {
                ++m_stats.m_npass;
            }
        }

        operator TestStats() const
        { return m_stats; }
};

#define check(cond) _check(Test::Location{__LINE__, __FILE__, #cond}, (cond))
#define checkEQ(a, b)                                                        \
    _check(Test::Location{__LINE__, __FILE__, #a " == " #b}, (a) == (b))
#define checkNE(a, b)                                                        \
    _check(Test::Location{__LINE__, __FILE__, #a " != " #b}, (a) != (b))
#define checkLT(a, b)                                                        \
    _check(Test::Location{__LINE__, __FILE__, #a " < " #b}, (a) < (b))
#define checkGT(a, b)                                                        \
    _check(Test::Location{__LINE__, __FILE__, #a " > " #b}, (a) > (b))
#define checkLE(a, b)                                                        \
    _check(Test::Location{__LINE__, __FILE__, #a " <= " #b}, (a) <= (b))
#define checkGE(a, b)                                                        \
    _check(Test::Location{__LINE__, __FILE__, #a " >= " #b}, (a) >= (b))

#define ign_check(cond)                                                      \
    _check(Test::Location{__LINE__, __FILE__, #cond}, (cond), true)
#define ign_checkEQ(a, b)                                                    \
    _check(Test::Location{__LINE__, __FILE__, #a " == " #b}, (a) == (b), true)
#define ign_checkNE(a, b)                                                    \
    _check(Test::Location{__LINE__, __FILE__, #a " != " #b}, (a) != (b), true)
#define ign_checkLT(a, b)                                                    \
    _check(Test::Location{__LINE__, __FILE__, #a " < " #b}, (a) < (b), true)
#define ign_checkGT(a, b)                                                    \
    _check(Test::Location{__LINE__, __FILE__, #a " > " #b}, (a) > (b), true)
#define ign_checkLE(a, b)                                                    \
    _check(Test::Location{__LINE__, __FILE__, #a " <= " #b}, (a) <= (b), true)
#define ign_checkGE(a, b)                                                    \
    _check(Test::Location{__LINE__, __FILE__, #a " >= " #b}, (a) >= (b), true)

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
