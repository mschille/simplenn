/** @file Test.cc
 *
 * @brief very simple unit testing framework
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2019-08-08
 *
 * For copyright and license information, see the end of the file.
 */
#include <iostream>

#include "Test.h"

Test::Test(const char* name) : m_name(name) {}

Test::~Test()
{
    for (const auto& loc : m_fails) {
        std::cout << m_name << ": In " << loc.m_file << ", line "
                  << loc.m_line << ": Violated condition " << loc.m_condstr
                  << "\n\n";
    }
    if (m_stats.m_nfail) {
        std::cout << m_name << ": " << m_stats.m_nfail << "/"
                  << (m_stats.m_nfail + m_stats.m_npass) << " ("
                  << (100.f * float(m_stats.m_nfail) /
                      float(m_stats.m_nfail + m_stats.m_npass))
                  << "% failed)" << std::endl;
    } else {
        std::cout << m_name << ": All " << m_stats.m_npass << " tests passed."
                  << std::endl;
    }
}

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
