/** @file tests_gru_v2.h
 *
 * @brief unit tests for GRU NN layer
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-11-24
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include <array>
#include <cmath>
#include <limits>

#include "Test.h"

#include "simplenn/processing.h"
#include "simplenn/GRU.h"

TestStats test_gru_v2()
{
    Test t("GRUv2");

#if 0
    float bias_0[2][12] = {
        { +0.19569,  -0.322547, -0.365777, -0.494326, -0.583687, -0.624228, +0.584192, -0.387587, -0.12293, -0.0073967, -0.416753, +0.522299 },
        { -0.267734, -0.520337, -0.416509, +0.460957, -0.651851, -0.144929, -0.123738, +0.587348, +0.18538, +0.218618,  +0.272966, -0.575096 }
    };
    float kernel_0[3][12] = {
        { +0.282668,  -0.509179, +0.616699, +0.606736, +0.421735, +0.378334, -0.34452,  -0.395756,  +0.199772, +0.0746341, -0.266819,  +0.35225 },
        { +0.0662972, +0.531379, -0.485207, -0.190687, +0.598396, -0.593919, +0.267982, +0.284715,  +0.265935, +0.4588,    +0.282459,  +0.375716 },
        { +0.368722,  -0.508086, +0.414954, +0.52333,  -0.506725, -0.451365, +0.276081, +0.0210286, +0.507632, -0.409881,  -0.0701684, +0.157947 }
    };
    float recurrent_kernel_0[4][12] = {
        { -0.39427,   +0.310463, -0.287918,  -0.660494,  +0.232084,  +0.29006,   -0.0298355, +0.0890315, -0.02727,  +0.0654644, -0.0841256, -0.264752 },
        { -0.0736199, +0.422921, +0.0328806, +0.0435011, +0.0114679, -0.482019,  -0.249767,  +0.357327,  +0.52971,  +0.238545,  +0.228383,  +0.0234145 },
        { -0.301462,  -0.228291, -0.337323,  +0.246095,  +0.279646,  -0.344256,  -0.0605558, +0.26019,   -0.5672,   +0.23396,   +0.189385,  -0.0474846 },
        { -0.376695,  +0.316868, +0.179542,  +0.0634297, -0.582941,  -0.0305876, +0.261471,  +0.126523,  -0.276033, +0.238081,  -0.326335,  +0.238612 }
    };
#endif
    using namespace simpleNN;
    static GRU<4, 3, procs::tanh, procs::sigmoid, float, GRUv2> gru(
            // bz
            {+0.19569,  -0.322547, -0.365777, -0.494326 },
            // Uz
            {-0.39427,   +0.310463, -0.287918,  -0.660494,
            -0.0736199, +0.422921, +0.0328806, +0.0435011,
            -0.301462,  -0.228291, -0.337323,  +0.246095,
            -0.376695,  +0.316868, +0.179542,  +0.0634297 },
            // Wz
            { +0.282668,  -0.509179, +0.616699, +0.606736,
              +0.0662972, +0.531379, -0.485207, -0.190687,
              +0.368722,  -0.508086, +0.414954, +0.52333, },
            // br
            {-0.583687, -0.624228, +0.584192, -0.387587},
            // Ur
             { +0.232084,  +0.29006,   -0.0298355, +0.0890315,
              +0.0114679, -0.482019,  -0.249767,  +0.357327,
              +0.279646,  -0.344256,  -0.0605558, +0.26019,
              -0.582941,  -0.0305876, +0.261471,  +0.126523, },
            // Wr
             { +0.421735, +0.378334, -0.34452,  -0.395756,
               +0.598396, -0.593919, +0.267982, +0.284715,
               -0.506725, -0.451365, +0.276081, +0.0210286, },
            // bh
            { -0.12293, -0.0073967, -0.416753, +0.522299},
            // Uh
            { -0.02727,  +0.0654644, -0.0841256, -0.264752,
              +0.52971,  +0.238545,  +0.228383,  +0.0234145,
              -0.5672,   +0.23396,   +0.189385,  -0.0474846,
              -0.276033, +0.238081,  -0.326335,  +0.238612 },
            // Wh
            { +0.199772, +0.0746341, -0.266819,  +0.35225,
              +0.265935, +0.4588,    +0.282459,  +0.375716,
              +0.507632, -0.409881,  -0.0701684, +0.157947 });

    std::array<float, 3> inputs[] = {
            {{0.4853260954323819, 0.5967655627456097, 0.2688290982251912}},
            {{0.27559256144090327, 0.34026808047996304, 0.7188723732297105}}};
    const std::array<float, 4> outputs[] = {
            {{0.26689448952674866, 0.007833652198314667, -0.2425648272037506,
              0.35144248604774475}},
            {{0.26689448952674866, 0.007833652198314667, -0.2425648272037506,
              0.35144248604774475}}};

    auto eval = gru.evaluator();
    auto it = std::begin(outputs);
    for (const auto& features : inputs) {
        eval << features;
        t.checkEQ(eval.size(), 4u);
        {
            const auto& output = *it++;
            for (unsigned i = 0; eval.size() != i; ++i) {
                //std::printf("i=%u eval %f out %f\n", i, eval[i], output[i]);
                t.ign_checkLT(std::abs(eval[i] - output[i]),
                              64.f * std::abs(output[i]) *
                                      std::numeric_limits<float>::epsilon());
            }
        }
    }
    return t;
}

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
