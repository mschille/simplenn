/** @file tests_gru_v2.h
 *
 * @brief unit tests for GRU NN layer
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2020-11-24
 *
 * For copyright and license information, see the end of the file.
 */
#pragma once

#include <array>
#include <cmath>
#include <limits>

#include "Test.h"

#include "simplenn/processing.h"
#include "simplenn/GRU.h"

TestStats test_gru_v1()
{
    Test t("GRUv1");

    using namespace simpleNN;
    static GRU<4, 3, procs::tanh, procs::sigmoid, float, GRUv1> gru(
            // bz
            { 0.278562, -0.310212, 0.0383338, 0.338459 },
            // Uz
            {-0.505923,   0.682683,  -0.0680375,  0.30622, 
             -0.138323,  -0.390893,  -0.317325,  -0.158628,
             -0.217011,   0.175482,  -0.0626429, -0.361877,
             -0.0413293,  0.0120158,  0.227868,  -0.410792},
            // Wz
            { -0.330769, -0.554505,   0.286882,  -0.00501537,
              -0.17978,   0.368604,   0.569497,   0.120168,  
               0.135354, -0.0694064, -0.0984365, -0.485785  },
            // br
            { 0.268404, 0.383741, -0.370223, -0.0351861 },
            // Ur
            { -0.0802481,  0.0188564, -0.0385691, -0.0402742, 
              -0.279445,   0.571455,   0.177537,  -0.219589,  
              -0.319406,   0.00622219, 0.104409,  -0.0759054, 
               0.120464,  -0.0693219,  0.344277,   0.608847  },
            // Wr
            { -0.237296,  0.464145, -0.402199, -0.386526,  
               0.189883,  0.18442,   0.27526,   0.176095,  
               0.388066, -0.447954,  0.146464, -0.0713137, },
            // bh
            { 0.0409658, -0.462109, 0.239507, 0.493386 },
            // Uh
            { 0.166046, -0.294458, -0.233754,  0.0276332,
              0.193155, -0.285082, -0.299084,  0.0989471,
              0.481733,  0.319071,  0.57719,   0.0385986,
              0.21801,  -0.338677, -0.141869, -0.294243 },
            // Wh
            {  0.462098, -0.352995, 0.472046,  0.227121,
              -0.397108, -0.185833, 0.19839,   0.402936,
              -0.173266,  0.298895, 0.142925, -0.315269 }
    );

    std::array<float, 3> inputs[] = {
            {{0.4853260954323819, 0.5967655627456097, 0.2688290982251912}},
            {{0.27559256144090327, 0.34026808047996304, 0.7188723732297105}}};
    const std::array<float, 4> outputs[] = {
            {{-0.02707022614777088, -0.35237783193588257, 0.3492804169654846,
              0.33538368344306946}},
            {{-0.02707022614777088, -0.35237783193588257, 0.3492804169654846,
              0.33538368344306946}}};

    auto eval = gru.evaluator();
    auto it = std::begin(outputs);
    for (const auto& features : inputs) {
        eval << features;
    t.checkEQ(eval.size(), 4u);
    {
        const auto& output = *it++;
        for (unsigned i = 0; eval.size() != i; ++i) {
            //std::printf("i=%u eval %f out %f\n", i, eval[i], output[i]);
            t.ign_checkLT(std::abs(eval[i] - output[i]),
                    64.f * std::abs(output[i]) *
                            std::numeric_limits<float>::epsilon());
        }
    }
    }
    return t;
}

/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

// vim: sw=4:tw=78:ft=cpp:et
